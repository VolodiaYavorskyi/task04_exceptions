package com.vova.passive;

import java.util.ArrayList;
import com.vova.passive.view.MyView;
import com.vova.passive.model.*;
import com.vova.passive.controller.*;

public class Application {
	
	public static void main(String[] args) {
		ArrayList<Shipping> testShippings = new ArrayList<>();
		Offers testOffers = new Offers();
		testOffers.setShippings(testShippings);
		
		testShippings.add(new Shipping("Boston", "New York", 100, 20, 10));
		testShippings.add(new Shipping("Campton", "New York", 100, 50, 5));
		testShippings.add(new Shipping("Orlean", "New York", 200, 30, 9));
		testShippings.add(new Shipping("Toronto", "Washington", 800, 200, 30));
		testShippings.add(new Shipping("New Jearsey", "New York", 50, 15, 7));
		
		new MyView(new MyController(new BusinessLogic(testOffers))).show();
	}
}

package com.vova.passive.model;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Shipping {
	private String sender;
	private String destination;
	private int maxKg;
	private int price;
	private int daysToReceive;
	
	public Shipping(String sender, String destination, int maxKg, int price, int daysToReceive) {
		this.sender = sender;
		this.destination = destination;
		this.maxKg = maxKg;
		this.price = price;
		this.daysToReceive = daysToReceive;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public int getMaxKg() {
		return maxKg;
	}

	public void setMaxKg(int maxKg) {
		this.maxKg = maxKg;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getDaysToReceive() {
		return daysToReceive;
	}

	public void setDaysToReceive(int daysToReceive) {
		this.daysToReceive = daysToReceive;
	}
	
	public static void writeToFile(Shipping s) throws IOException {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter("ShippingWithMaxPrice.txt"))) {			
			writer.write(s.toString());
		}
	}
	
	@Override
	public String toString() {
		return "Shipping [sender=" + sender + ", destination=" + destination + ", maxKg=" + maxKg + ", price=" + price
				+ ", daysToReceive=" + daysToReceive + "]";
	}

}

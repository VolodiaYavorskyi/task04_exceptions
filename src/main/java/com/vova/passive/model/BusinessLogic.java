package com.vova.passive.model;

import java.io.IOException;
import java.util.ArrayList;

public class BusinessLogic implements Model {
	private Offers offers;
	
	public BusinessLogic(Offers offers) {
		this.offers = offers;
	}
	
	@Override
	public ArrayList<Shipping> getOffers() {
		return offers.getShippings();
	}

	@Override
	public Shipping getWithMaxPrice() {
		return offers.getWithMaxPrice();
	}

	@Override
	public void sortByPrice() {
		offers.sortByPrice();
	}

	@Override
	public void sortByTime() {
		offers.sortByTime();
	}

	@Override
	public void writeShippingToFile(Shipping s) {
		try {
			Shipping.writeToFile(s);
		} catch (IOException e) {
			System.out.println("Error while writing to file");
		}
	}
}

package com.vova.passive.model;

import java.util.ArrayList;
import com.vova.passive.NoShippingsException;

public class Offers {
	private ArrayList<Shipping> shippings;
	private Shipping withMaxPrice;

	public Offers() {
		shippings = new ArrayList<>();
	}

	public Shipping getWithMaxPrice() throws NoShippingsException {
		if (shippings.isEmpty()) {
			throw new NoShippingsException("There are no shippings yet");
		}
		
		withMaxPrice = shippings.get(0);
		
		for (Shipping s : shippings) {
			if (s.getPrice() > withMaxPrice.getPrice()) {
				withMaxPrice = s;
			}
		}
		
		return withMaxPrice;
	}
	
	public void sortByPrice() throws NoShippingsException {
		int n = shippings.size();
		
		if (n == 0) {
			throw new NoShippingsException("There are no shippings yet");
		}
		
		for (int i = 0; i < n; i++) {
			for (int j = 1; j < n - i; j++) {
				if (shippings.get(j).getPrice() 
						< shippings.get(j - 1).getPrice()) {
					Shipping temp = shippings.get(j);
					shippings.set(j, shippings.get(j - 1));
					shippings.set(j - 1, temp);
				}
			}
		}
	}
	
	public void sortByTime() throws NoShippingsException {
		int n = shippings.size();
		
		if (n == 0) {
			throw new NoShippingsException("There are no shippings yet");
		}
		
		for (int i = 0; i < n; i++) {
			for (int j = 1; j < n - i; j++) {
				if (shippings.get(j).getDaysToReceive() 
						< shippings.get(j - 1).getDaysToReceive()) {
					Shipping temp = shippings.get(j);
					shippings.set(j, shippings.get(j - 1));
					shippings.set(j - 1, temp);
				}
			}
		}
	}
	
	public void addShipping(Shipping s) {
		shippings.add(s);
	}
	
	public ArrayList<Shipping> getShippings() {
		return shippings;
	}

	public void setShippings(ArrayList<Shipping> shippings) {
		this.shippings = shippings;
	}
}

package com.vova.passive;

public class NoShippingsException extends RuntimeException {
	private static final long serialVersionUID = -5820551621667014824L;

	public NoShippingsException(String message) {
		super(message);
	}
	
	public NoShippingsException(Throwable cause) {
		super(cause);
	}
	
	public NoShippingsException(String message, Throwable cause) {
		super(message, cause);
	}
}

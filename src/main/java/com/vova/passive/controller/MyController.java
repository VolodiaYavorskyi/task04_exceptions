package com.vova.passive.controller;

import java.util.ArrayList;

import com.vova.passive.model.*;

public class MyController implements Controller {
	
	private Model model;
	
	public MyController(Model model) {
		this.model = model;
	}
	
	@Override
	public ArrayList<Shipping> getOffers() {
		return model.getOffers();
	}

	@Override
	public Shipping getWithMaxPriceAndWriteToFile() {
		model.writeShippingToFile(model.getWithMaxPrice());
		return model.getWithMaxPrice();
	}

	@Override
	public ArrayList<Shipping> getSortedByPrice() {
		model.sortByPrice();
		return model.getOffers();
	}

	@Override
	public ArrayList<Shipping> getSortedByTime() {
		model.sortByTime();
		return model.getOffers();
	}
}
